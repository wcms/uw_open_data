<?php

/**
 * @file
 * uwaterloo_open_data.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uwaterloo_open_data_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uwaterloo_open_data_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uwaterloo_open_data_node_info() {
  $items = array(
    'open_data' => array(
      'name' => t('Open Data Catalogue'),
      'base' => 'node_content',
      'description' => t('Custom Content Type for the Open Data Project at University of Waterloo'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'request_data_set' => array(
      'name' => t('Request Data Set'),
      'base' => 'node_content',
      'description' => t('Authenicated User may create a request for a new data set.   '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
