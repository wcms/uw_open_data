<?php

/**
 * @file
 * uwaterloo_open_data.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uwaterloo_open_data_user_default_permissions() {
  $permissions = array();

  // Exported permission: create request_data_set content.
  $permissions['create request_data_set content'] = array(
    'name' => 'create request_data_set content',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
