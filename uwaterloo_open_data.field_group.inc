<?php

/**
 * @file
 * uwaterloo_open_data.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uwaterloo_open_data_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_file_types|node|open_data|default';
  $field_group->group_name = 'group_file_types';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'open_data';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'File Types Available',
    'weight' => '13',
    'children' => array(
      0 => 'field_file_type_xml',
      1 => 'field_file_type_json',
      2 => 'field_file_type_csv',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'File Types Available',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_file_types|node|open_data|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_file_types|node|open_data|form';
  $field_group->group_name = 'group_file_types';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'open_data';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'File Types Available',
    'weight' => '8',
    'children' => array(
      0 => 'field_file_type_xml',
      1 => 'field_file_type_json',
      2 => 'field_file_type_csv',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_file_types|node|open_data|form'] = $field_group;

  return $export;
}
